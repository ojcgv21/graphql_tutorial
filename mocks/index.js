const { authors } = require('./authors.json');
const { books } = require('./books.json');

module.exports = {
  authors,
  books
};
